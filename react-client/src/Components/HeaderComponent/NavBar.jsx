import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NavBar extends Component {
  render() {
    return (
      <header>
        <ul id="headerButtons">
          <li className="navButton"><a href=""><img src={require('./logosmall.png')} /></a></li>
        </ul>
      </header>
    )
  }
}

export default NavBar;
