import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer>
        <div>
          Copyright © 2018 phelps.ws<br />
        </div>
      </footer>
    )
  }
}

export default Footer;
