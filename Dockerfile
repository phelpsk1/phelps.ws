#base image
FROM node:latest

#set working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH

#install and cache app dependencies
#COPY package.json /usr/src/app/package.json
COPY . /usr/src/app
RUN npm install --silent
RUN npm install react-scripts -g --silent

#start app
CMD ["npm", "start"]
